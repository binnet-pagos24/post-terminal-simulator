import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { VoucherDialogComponent } from './shared/components/voucher-dialog/voucher-dialog.component';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthComponent } from './shared/components/auth/auth.component';
import { IUser } from './shared/interfaces/user.interface.';
import { IHtmlCode } from './shared/interfaces/htmlCode.interface';
import { EStorage } from './shared/enums/EStorage';
import Pagos24 from 'pagos24-js';
import { LanguageType } from 'pagos24-js/types/LanguageType';
import { IPOS } from 'pagos24-js/interfaces/IPOS';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public paymentForm: FormGroup;
  public refundForm: FormGroup;
  public discoveredReaders: IPOS[];
  public htmlCodes: IHtmlCode[];
  public isConnected: boolean;
  public currentPaymentId: string;
  public intervalPay: number;
  public timePay: string;
  public showVoucher: boolean;
  public sandbox: boolean;
  public currentUser: IUser;
  public currentReader: IPOS;
  public enableCancelPayment: boolean;
  public showLoading: boolean;
  public simulatorChecked: boolean;

  private latitud: string;
  private longitud: string;
  private voucherString: string;
  private pagos24: Pagos24;

  constructor(private formBuilder: FormBuilder, private dialog: MatDialog) {
    this.paymentForm = this.formBuilder.group({
      amount: [null, Validators.required],
      description: [null, Validators.required],
    });
    this.refundForm = this.formBuilder.group({
      paymentId: [null, Validators.required],
    });
    this.htmlCodes = [];
    this.isConnected = false;
    this.currentPaymentId = undefined;
    this.timePay = '';
    this.currentReader = undefined;
    this.latitud = undefined;
    this.longitud = undefined;
    this.showVoucher = false;
    this.sandbox = true;
    this.enableCancelPayment = false;
    this.showLoading = false;
    this.simulatorChecked = true;
  }

  ngOnInit(): void {
    navigator.geolocation.getCurrentPosition((success) => {
      this.latitud = String(success.coords.latitude);
      this.longitud = String(success.coords.longitude);
    }, console.error);

    const localUSer = localStorage.getItem(EStorage.user);
    if (localUSer) {
      this.currentUser = JSON.parse(atob(localUSer));
    }
    this.openAuth();
  }

  /**
   * @description Started Pagos24
   * @param {boolean} simulated
   * @param {boolean} sandbox
   */
  public startP24(): void {
    this.discoveredReaders = [];
    this.pagos24 = new Pagos24({
      appId: this.currentUser.appId,
      email: this.currentUser.email,
      lang: this.currentUser.lang as LanguageType,
      projectUrl: this.currentUser.url,
      sandbox: this.sandbox,
      secretKey: this.currentUser.secretKey,
      token: this.currentUser.tokenApi,
    });
    localStorage.setItem(EStorage.user, btoa(JSON.stringify(this.currentUser)));
    this.pagos24.pos
      .list({
        idLocation: this.currentUser.terminalLocation,
        simulated: this.simulatorChecked,
      })
      .then((list) => {
        const title = 'Lista de POS';
        const code = list;
        this.insertJSON({
          data: [{ code, title, transactionId: this.currentPaymentId }],
        });

        this.discoveredReaders = list;
      })
      .catch((err) => {
        const title = 'GET POS ERROR';
        const code = err;
        this.insertJSON({
          data: [{ code, title, transactionId: this.currentPaymentId }],
        });
      });
  }

  /**
   * @description Change sandox toggle
   * @param {MatSlideToggleChange} event
   * @return {void}
   */
  public changeSandbox(event: MatSlideToggleChange): void {
    const { checked } = event;
    this.sandbox = checked;
    this.startP24();
  }

  /**
   * @description Change simulator toggle
   * @param {MatSlideToggleChange} event
   * @return {void}
   */
  public chnageSimulator(event: MatSlideToggleChange): void {
    const { checked } = event;
    this.simulatorChecked = checked;
    if (checked) {
      this.sandbox = true;
    }
    this.startP24();
  }

  /**
   * @description Connect to POS24
   * @param {Reader} pos
   * @return {void}
   */
  public connectToPOS24(pos: IPOS): void {
    this.showLoading = true;
    this.pagos24.pos
      .connectTo(pos)
      .then((currentPOS) => {
        const title = 'Connect to POS';
        const code = pos;
        this.insertJSON({
          data: [{ code, title, transactionId: this.currentPaymentId }],
        });
        this.currentReader = currentPOS;
        this.isConnected = true;
      })
      .catch((err) => {
        const title = 'Connect to POS ERROR';
        const code = err;
        this.insertJSON({
          data: [{ code, title, transactionId: this.currentPaymentId }],
        });
      })
      .finally(() => (this.showLoading = false));
  }

  /**
   * @description Open auth modal
   * @return {void}
   */
  public openAuth(): void {
    const dialofRef = this.dialog.open(AuthComponent, {
      width: '400px',
      autoFocus: false,
      data: this.currentUser,
    });

    dialofRef.afterClosed().subscribe((resp) => {
      if (resp) {
        this.currentUser = resp;
        this.startP24();
      }
    });
  }

  /**
   * @description Payment with POS24
   * @return {void}
   */
  public paymentPOS24(): void {
    this.showLoading = true;
    const { amount, description } = this.paymentForm.value;
    this.pagos24.pos
      .createPaymentIntent({
        amount: Number(amount),
        description,
      })
      .then((createPaymentResponse) => {
        const title = 'Create Payment';
        const code = createPaymentResponse;
        this.currentPaymentId = createPaymentResponse.id;
        this.insertJSON({
          data: [{ code, title, transactionId: this.currentPaymentId }],
        });
        this.pagos24.pos
          .capturePaymentIntent({
            posId: this.currentReader.id,
            paymentIntentId: this.currentPaymentId,
            position: {
              latitude: this.latitud,
              longitude: this.longitud,
            },
          })
          .then((captureResponse) => {
            const title = 'Capture Payment';
            const code = captureResponse;
            this.currentPaymentId = createPaymentResponse.id;
            this.insertJSON({
              data: [{ code, title, transactionId: this.currentPaymentId }],
            });
            this.voucherString = captureResponse.voucher;
            this.showVoucher = true;
          })
          .catch((err) => {
            const title = 'Capture Payment ERROR';
            const code = err;
            this.insertJSON({
              data: [{ code, title, transactionId: this.currentPaymentId }],
            });
          })
          .finally(() => (this.showLoading = false));
      })
      .catch((err) => {
        const title = 'Create Payment ERROR';
        const code = err;
        this.insertJSON({
          data: [{ code, title, transactionId: this.currentPaymentId }],
        });
        this.showLoading = false;
      });
  }

  /**
   * @description Make to clos epos
   * @return {void}
   */
  public closePos(): void {
    this.showLoading = true;
    this.pagos24.pos
      .closing(this.currentReader.id)
      .then((closingResponse) => {
        const title = 'Closing POS';
        const code = closingResponse;
        this.insertJSON({
          data: [{ code, title, transactionId: undefined }],
        });
      })
      .catch((err) => {
        const title = 'Closing ERROR';
        const code = err;
        this.insertJSON({
          data: [{ code, title, transactionId: undefined }],
        });
      })
      .finally(() => (this.showLoading = false));
  }

  /**
   * @description Open voucher dialog
   * @return {void}
   */
  public openVoucherDialog(): void {
    this.dialog.open(VoucherDialogComponent, {
      data: this.voucherString,
    });
  }

  /**
   * @description Initializes the payment timer
   * @return {void}
   */
  public startPayTime(): void {
    clearInterval(this.intervalPay);
    let tenMinutesInSeg: number = 300;
    let minutes: number = 5;
    let seg: number = 0;

    // @ts-ignore
    this.intervalPay = setInterval(() => {
      tenMinutesInSeg--;
      if (seg === 0) {
        minutes--;
        seg = 59;
        const time = `${minutes}:${
          String(seg).length === 1 ? '0' + String(seg) : String(seg)
        }`;
        this.timePay = time;
      } else {
        seg--;
        const time = `${minutes}:${
          String(seg).length === 1 ? '0' + String(seg) : String(seg)
        }`;
        this.timePay = time;
      }

      if (tenMinutesInSeg === 0) {
        clearInterval(this.intervalPay);
      }
    }, 1000);
  }

  /**
   * @description Refund a Payment Intent
   * @return {void}
   */
  public refund(): void {
    this.showLoading = true;
    const { paymentId } = this.refundForm.value;
    this.pagos24.pos
      .refund(paymentId)
      .then((refundResponse) => {
        const title = 'Create Payment';
        const code = refundResponse;
        this.insertJSON({
          data: [{ code, title, transactionId: paymentId }],
        });
      })
      .catch((err) => {
        const title = 'Refund Payment ERROR';
        const code = err;
        this.insertJSON({
          data: [{ code, title, transactionId: this.currentPaymentId }],
        });
      })
      .finally(() => (this.showLoading = false));
  }

  /**
   * @description Disconnect current Reader
   * @return {void}
   */
  public disconnectDevice(): void {
    this.pagos24.pos.disconnect();
    this.currentReader = undefined;
  }

  /**
   * @description Creates line breaks in the console text
   * @param stringJSON JSON in string format
   * @return {void}
   */
  public insertJSON(code: IHtmlCode): void {
    this.htmlCodes.unshift(code);
  }
}
