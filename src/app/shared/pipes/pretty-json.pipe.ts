import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prettyJson',
})
export class PrettyJsonPipe implements PipeTransform {
  transform(val) {
    const stringJSON = JSON.stringify(val, undefined, 4);
    return this.stripHtml(stringJSON)
      .replace(/ /g, '&nbsp;')
      .replace(/\n/g, '<br/>')
      .replace(/:/g, ': ');
  }

  /**
   * @description Remove html elements and return only text
   * @param html {string}
   * @return {string}
   */
  private stripHtml(html: string): string {
    const temporalDivElement = document.createElement('div');
    const regExp = new RegExp(
      '</div>|</h1>|</h2>|</h3>|</h4>|</h5>|</h6>|</h7>|</p>|</pre>',
      'gi'
    );
    temporalDivElement.innerHTML = html.replace(regExp, '\n');

    return temporalDivElement.textContent || temporalDivElement.innerText || '';
  }
}
