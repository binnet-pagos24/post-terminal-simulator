import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ELangTypes } from '../../enums/ELangTypes';
import { IUser } from '../../interfaces/user.interface.';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  public authForm: FormGroup;
  public langOptions: typeof ELangTypes;

  constructor(
    public dialogRef: MatDialogRef<AuthComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public user?: IUser
  ) {
    this.authForm = this.formBuilder.group({
      email: [user?.email || null, Validators.required],
      secretKey: [user?.secretKey || null, Validators.required],
      appId: [user?.appId || null, Validators.required],
      tokenApi: [user?.tokenApi, Validators.required],
      terminalLocation: [user?.terminalLocation || null, Validators.required],
      url: [user?.url || null, Validators.required],
      lang: [user?.lang || null, Validators.required],
    });
    this.langOptions = ELangTypes;
  }

  ngOnInit(): void {}

  public auth(): void {
    this.dialogRef.close(this.authForm.value);
  }
}
