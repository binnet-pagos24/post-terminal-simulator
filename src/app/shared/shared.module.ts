import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { VoucherDialogComponent } from './components/voucher-dialog/voucher-dialog.component';
import { AuthComponent } from './components/auth/auth.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingComponent } from './components/loading/loading.component';
import { PrettyJsonPipe } from './pipes/pretty-json.pipe';

@NgModule({
  declarations: [
    VoucherDialogComponent,
    AuthComponent,
    LoadingComponent,
    PrettyJsonPipe,
  ],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule],
  exports: [MaterialModule, LoadingComponent, PrettyJsonPipe],
})
export class SharedModule {}
