import { HmacSHA256 } from 'crypto-js';

/**
 * @description Format the data that will be sent
 * @param data JSON to send
 * @return btoa
 */
export const getPlayloadFormated = (data: {}, secretKey?: string): string => {
  const dataB64 = btoa(JSON.stringify(data));
  return dataB64 + (secretKey ? '.' + HmacSHA256(dataB64, secretKey) : '');
};

/**
 * @description Get auth format
 * @param email User email
 * @param token User token Api
 * @returns string
 */
export const getAuth = (email: string, token: string): string => {
  return `Basic ${btoa(email.trim().toLowerCase() + ':' + token)}`;
};
