export interface IHttpResponse<T> {
  statusCode: number;
  message: string;
  data: T;
  count: number;
  errors: null | {};
}
