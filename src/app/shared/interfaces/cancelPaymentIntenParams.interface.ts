import { IUser } from './user.interface.';

export interface ICancelPaymentIntentParams {
  paymentIntentId: string;
  sandbox: boolean;
  secretKey: string;
  appId: string;
  user: IUser;
}
