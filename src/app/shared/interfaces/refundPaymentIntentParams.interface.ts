import { IUser } from './user.interface.';

export interface IRefundPaymentIntentParams {
  paymentIntentId: string;
  appId: string;
  sandbox: boolean;
  secretKey: string;
  user: IUser;
}
