import { ISdkManagedPaymentIntent } from '@stripe/terminal-js';

export interface ISDKPaymentIntent extends ISdkManagedPaymentIntent {
  error: object | null;
  sdk_payment_details: object;
  paymentIntent: ISdkManagedPaymentIntent;
}
