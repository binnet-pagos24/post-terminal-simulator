import { ELangTypes } from '../enums/ELangTypes';
import { IUser } from './user.interface.';

export interface ICapturePaymentIntent {
  paymentIntentId: string;
  posId: string;
  latitude: string;
  longitude: string;
  lang: ELangTypes;
  sandbox: boolean;
  url: string;
  secretKey: string;
  user: IUser;
  appId: string;
}
