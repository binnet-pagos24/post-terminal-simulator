export interface IRefundResponse {
  charge?: string;
  amount?: number;
  currency?: string;
}
