import { CurrencyType } from '../types/currency.type';
import { IUser } from './user.interface.';

export interface ICreatePaymentIntentParams {
  amount: number;
  currency: CurrencyType;
  description: string;
  appId: string;
  secretKey: string;
  user: IUser;
  sandbox: boolean;
}
