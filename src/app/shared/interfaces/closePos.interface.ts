import { IUser } from './user.interface.';

export interface IClosePos {
  posId: string;
  secrectKey: string;
  appId: string;
  user: IUser;
}
