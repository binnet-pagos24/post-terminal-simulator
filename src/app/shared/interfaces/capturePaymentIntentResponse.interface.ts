export interface ICapturePaymentIntentResponse {
  txt_msg: string;
  title: string;
  trasnsaction_id: string;
  payment_intent_id: string;
  amount: string;
  voucher: string;
  error: number;
}
