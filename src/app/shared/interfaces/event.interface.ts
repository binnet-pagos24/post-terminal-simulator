export interface IEvent {
  authorization: string;
  data: string;
  appId?: string;
}
