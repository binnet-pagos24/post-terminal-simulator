import { ELangTypes } from '../enums/ELangTypes';

export interface IUser {
  appId: string;
  email: string;
  secretKey: string;
  terminalLocation: string;
  tokenApi: string;
  url: string;
  lang: ELangTypes;
}
