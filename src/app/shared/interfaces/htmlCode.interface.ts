export interface IHtmlCode {
  data: { code: any; title: string; transactionId: string }[];
}
