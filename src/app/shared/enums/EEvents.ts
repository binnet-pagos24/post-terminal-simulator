export enum EEvents {
  Connection = 'connect',
  CreatePaymentIntent = 'createPaymentIntent',
  CapturePaymentIntent = 'capturePaymentIntent',
  RefundPaymentIntent = 'refundPaymentIntent',
  GetPaymentIntent = 'getPaymentIntent',
}
