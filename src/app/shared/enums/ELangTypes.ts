export enum ELangTypes {
    EsVe = 'es-VE',
    EnUs = 'en-US',
    PtBr = 'pt-BR',
    No = 'NO'
}
